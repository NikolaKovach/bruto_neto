package com.developer.nikolakovach.brutoneto;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import com.developer.nikolakovach.brutoneto.fragments.Calc_summary_portreit;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    ToggleButton gross, net;
    LinearLayout linearLayout;

    boolean doubleBackToExitPressedOnce = false;


    //admob
    private static final String TAG = "MainActivity";
    private AdView mAdView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        MobileAds.initialize(this, "ca-app-pub-8363328495832095~9787078428");

        ///ADMOB
        mAdView =  findViewById(R.id.adView);

        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        initcomponents();
        setcomponents();

    }

    private void setcomponents() {

        gross.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (gross.isChecked()){
                    net.setChecked(false);
                }

                if (gross.isChecked()){
                    BooleanHelper.getInstance().grosssss=true;
                    BooleanHelper.getInstance().nettttt=false;


                }
                if (gross.isChecked()==false){
                    BooleanHelper.getInstance().grosssss=false;
                }
                System.out.println(BooleanHelper.getInstance().grosssss);
                System.out.println(BooleanHelper.getInstance().nettttt);
            }
        });

        net.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (net.isChecked()){
                    gross.setChecked(false);
                }
                if (net.isChecked()){
                    BooleanHelper.getInstance().nettttt=true;
                    BooleanHelper.getInstance().grosssss=false;
                }

                if (net.isChecked()==false){
                    BooleanHelper.getInstance().nettttt=false;
                }

                System.out.println(BooleanHelper.getInstance().grosssss);
                System.out.println(BooleanHelper.getInstance().nettttt);
            }
        });

    }



    private void initcomponents() {
        linearLayout = findViewById(R.id.linearlay);






        getSupportFragmentManager().beginTransaction().replace(R.id.container, new  Calc_summary_portreit()).commit();


        gross = findViewById(R.id.grosbtn);
        net = findViewById(R.id.netbtn);


        Locale current = getResources().getConfiguration().locale;
        System.out.println(current);

    }




    @Override
    public void onBackPressed() {



        Snackbar snackbar =Snackbar.make(findViewById(R.id.linearlay), "Please click BACK again to exit",
                Snackbar.LENGTH_SHORT);
                snackbar.show();
        View view = snackbar.getView();
        view.setBackgroundColor(Color.GRAY);

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
//        Toast.makeText(this, "", Toast.LENGTH_SHORT).show();

//
//        Intent intent = new Intent(MainActivity.this, MainActivity.class);
//        startActivity(intent);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new  Calc_summary_portreit()).commit();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);








    }
}
