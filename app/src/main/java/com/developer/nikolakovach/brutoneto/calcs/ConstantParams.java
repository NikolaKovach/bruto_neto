package com.developer.nikolakovach.brutoneto.calcs;

/**
 * Created by Nikola Kovacevski on 12-Mar-18.
 */

public class ConstantParams {

    private int year= 2018;
    private double personalIncomeTaxRate= 0.1; //personalen danok
    private double pensionAndDisabilityInsuranceRate= 0.18;
    private double healthInsuranceFundRate= 0.073; // zdravstveno
    private double additionalHealthInsuranceAndPersonalInjuryInsuranceRate= 0.005; //dopolnitelno zdravstveno
    private double unemploymentInsuranceFundRate= 0.012;    //zdravstveno vo slucaj na nevrabotenost
    private int personalIncomeTaxExemptionPerMonth= 7531; //
    private int averageSalary= 34079;   //prosecna plata

    public ConstantParams(){}

    public ConstantParams(double personalIncomeTaxRate, double pensionAndDisabilityInsuranceRate, double healthInsuranceFundRate, double additionalHealthInsuranceAndPersonalInjuryInsuranceRate, double unemploymentInsuranceFundRate, int personalIncomeTaxExemptionPerMonth, int averageSalary) {
        this.personalIncomeTaxRate = personalIncomeTaxRate;
        this.pensionAndDisabilityInsuranceRate = pensionAndDisabilityInsuranceRate;
        this.healthInsuranceFundRate = healthInsuranceFundRate;
        this.additionalHealthInsuranceAndPersonalInjuryInsuranceRate = additionalHealthInsuranceAndPersonalInjuryInsuranceRate;
        this.unemploymentInsuranceFundRate = unemploymentInsuranceFundRate;
        this.personalIncomeTaxExemptionPerMonth = personalIncomeTaxExemptionPerMonth;
        this.averageSalary = averageSalary;
    }


    public double getPersonalIncomeTaxRate() {
        return personalIncomeTaxRate;
    }

    public double getPensionAndDisabilityInsuranceRate() {
        return pensionAndDisabilityInsuranceRate;
    }

    public double getHealthInsuranceFundRate() {
        return healthInsuranceFundRate;
    }

    public double getAdditionalHealthInsuranceAndPersonalInjuryInsuranceRate() {
        return additionalHealthInsuranceAndPersonalInjuryInsuranceRate;
    }

    public double getUnemploymentInsuranceFundRate() {
        return unemploymentInsuranceFundRate;
    }

    public int getPersonalIncomeTaxExemptionPerMonth() {
        return personalIncomeTaxExemptionPerMonth;
    }

    public int getAverageSalary() {
        return averageSalary;
    }
}
