package com.developer.nikolakovach.brutoneto.calcs;

import com.developer.nikolakovach.brutoneto.MainActivity;

import java.util.ArrayList;

/**
 * Created by Nikola Kovacevski on 11-Mar-18.
 */

public class Gross extends MainActivity {
    public Gross(String rezultat) {
        this.rezultat = rezultat;
    }

    public static ArrayList<String> getList() {
        return list;
    }

    public static ArrayList<String> list;
    private String rezultat;

    ConstantParams params = new ConstantParams();
    double personalIncomeTaxRate= params.getPersonalIncomeTaxRate();
    double pensionAndDisabilityInsuranceRate= params.getPensionAndDisabilityInsuranceRate();
    double healthInsuranceFundRate= params.getHealthInsuranceFundRate();
    double additionalHealthInsuranceAndPersonalInjuryInsuranceRate= params.getAdditionalHealthInsuranceAndPersonalInjuryInsuranceRate();
    double unemploymentInsuranceFundRate= params.getUnemploymentInsuranceFundRate();
    int personalIncomeTaxExemptionPerMonth= params.getPersonalIncomeTaxExemptionPerMonth();
    int averageSalary= 34079;



    public void presmetkaGross(){
        int bruto = Integer.parseInt(rezultat);

        double helthPension =
                         bruto*pensionAndDisabilityInsuranceRate//penzisko
                        + bruto*healthInsuranceFundRate//zdravstveno
                        + bruto*additionalHealthInsuranceAndPersonalInjuryInsuranceRate//dop zdrav
                        + bruto*unemploymentInsuranceFundRate;//nevrab


        double personalIncomeTax = (bruto -(helthPension+ personalIncomeTaxExemptionPerMonth))*personalIncomeTaxRate;



        double neto = bruto-helthPension-personalIncomeTax;

        list = new ArrayList<>();
        list.add(String.valueOf(Math.round(bruto)));
        list.add(String.valueOf(Math.round(bruto*pensionAndDisabilityInsuranceRate)));
        list.add(String.valueOf(Math.round(bruto*healthInsuranceFundRate)));
        list.add(String.valueOf(Math.round(bruto*additionalHealthInsuranceAndPersonalInjuryInsuranceRate)));
        list.add(String.valueOf(Math.round(bruto*unemploymentInsuranceFundRate)));
        list.add(String.valueOf(Math.round(helthPension)));
        list.add(String.valueOf(Math.round(personalIncomeTax)));
        list.add(String.valueOf(Math.round(helthPension+personalIncomeTax)));
        list.add(String.valueOf(Math.round(neto)));

        System.out.println(list);


    }
}
