package com.developer.nikolakovach.brutoneto.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.developer.nikolakovach.brutoneto.BooleanHelper;
import com.developer.nikolakovach.brutoneto.R;
import com.developer.nikolakovach.brutoneto.calcs.Gross;
import com.developer.nikolakovach.brutoneto.calcs.Net;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class Calc_summary_portreit extends Fragment {
    View v;
    TextView result;
    Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8,btn9, btn0, btnBackSpase, btnClear, btnOk, dot;

    Boolean grossssssss, nettttttttt;




    private InterstitialAd mInterstitialAd;

    StringBuilder numbers;

    public Calc_summary_portreit() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        MobileAds.initialize(getContext(), "ca-app-pub-8363328495832095~9787078428");

        mInterstitialAd = new InterstitialAd(getContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-8363328495832095/3464001202");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        System.out.println("reklamata e loadirana");

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_calc_summary_portreit, container, false);

        initcomponents();
        setcomponents();

        return v;
    }

    private void setcomponents() {

        getkeyboard();

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grossssssss = BooleanHelper.getInstance().grosssss;
                nettttttttt = BooleanHelper.getInstance().nettttt;

                if (grossssssss==nettttttttt){
                    Toast.makeText(getContext(), " Please select Gross or Net ", Toast.LENGTH_SHORT).show();
                }

                if (grossssssss==true &&nettttttttt==false && result.getText().toString()!="0"){
                    Gross gross = new Gross(result.getText().toString());
                   // result.setText(gross.presmetkaGross());
                    gross.presmetkaGross();
                    getFragmentManager().beginTransaction().replace(R.id.container, new PresmetkiGross()).commit();
                }
                System.out.println("u redu");

                if (nettttttttt==true && grossssssss==false && result.getText().toString()!="0"){
                    Net net = new Net(result.getText().toString());
                    net.presmetkaNet();
                    getFragmentManager().beginTransaction().replace(R.id.container, new PresmetkiNet()).commit();
                }

            }
        });

    }

    private void getkeyboard() {

        dot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //getFragmentManager().beginTransaction().remove(PieChart());
                System.out.println("vleze vo metod");
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                    System.out.println("izlez od reklama");
                } else {
                    System.out.println("vleze vo else");
                }
//                System.out.println("treba da izvadi toast");
//                Toast.makeText(getContext(), "AJ SO SREKJA!!!", Toast.LENGTH_LONG).show();



            }
        });


        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (result.getText() == "0"){
                    //numbers.append("0");
                    result.setText("0");
                }
                else {
                    if (numbers.length()<9){
                        numbers.append("0");
                        result.setText(numbers.toString());

                    }
                }
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numbers.length()<9){
                numbers.append("1");
                result.setText(numbers.toString());
                }
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numbers.length()<9) {
                    numbers.append("2");
                    result.setText(numbers.toString());
                }
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numbers.length()<9) {
                    numbers.append("3");
                    result.setText(numbers.toString());
                }
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numbers.length()<9){
                numbers.append("4");
                result.setText(numbers.toString());}
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numbers.length()<9){
                numbers.append("5");
                result.setText(numbers.toString());}
            }
        });

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numbers.length()<9){
                numbers.append("6");
                result.setText(numbers.toString());}
            }
        });

        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numbers.length()<9){
                numbers.append("7");
                result.setText(numbers.toString());}
            }
        });

        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numbers.length()<9) {
                    numbers.append("8");
                    result.setText(numbers.toString());
                }
            }
        });

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numbers.length()<9){
                numbers.append("9");
                result.setText(numbers.toString()); }
            }
        });


        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (numbers.length()<=9){
                numbers.delete(0, numbers.length());
                result.setText("0"); }
            }
        });

        btnBackSpase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (numbers.length()==1){
                    numbers.delete(0,1);
                    result.setText("0");
                }else
                if (numbers.length()>0){
                    int i =  numbers.length();
                    int j = numbers.length()-1;
                    numbers.delete(j,i);
                    result.setText(numbers.toString());
                    System.out.println(numbers.length());
                }else
                {
                    result.setText("0");
                }
            }
        });


    }

    private void initcomponents() {

        result = v.findViewById(R.id.restxt);
        btn0= v.findViewById(R.id.btn0);
        btn1= v.findViewById(R.id.btn1);
        btn2= v.findViewById(R.id.btn2);
        btn3= v.findViewById(R.id.btn3);
        btn4= v.findViewById(R.id.btn4);
        btn5= v.findViewById(R.id.btn5);
        btn6= v.findViewById(R.id.btn6);
        btn7= v.findViewById(R.id.btn7);
        btn8= v.findViewById(R.id.btn8);
        btn9= v.findViewById(R.id.btn9);
        btnBackSpase= v.findViewById(R.id.btnBackSpase);
        btnClear = v.findViewById(R.id.btnreset);
        btnOk = v.findViewById(R.id.btnCalc);
        dot = v.findViewById(R.id.btnDot);


        numbers = new StringBuilder();





    }

}
