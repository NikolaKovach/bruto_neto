package com.developer.nikolakovach.brutoneto.calcs;

import com.developer.nikolakovach.brutoneto.MainActivity;

import java.util.ArrayList;


public class Net extends MainActivity {

    public static ArrayList getNetlist() {
        return netlist;
    }

    public static ArrayList netlist;


    ConstantParams params = new ConstantParams();
    double personalIncomeTaxRate= params.getPersonalIncomeTaxRate();
    double pensionAndDisabilityInsuranceRate= params.getPensionAndDisabilityInsuranceRate();
    double healthInsuranceFundRate= params.getHealthInsuranceFundRate();
    double additionalHealthInsuranceAndPersonalInjuryInsuranceRate= params.getAdditionalHealthInsuranceAndPersonalInjuryInsuranceRate();
    double unemploymentInsuranceFundRate= params.getUnemploymentInsuranceFundRate();
    int personalIncomeTaxExemptionPerMonth= params.getPersonalIncomeTaxExemptionPerMonth();

    public Net(String rezultat) {
        this.rezultat = rezultat;
    }

    private String rezultat;

    public void presmetkaNet(){
        double neto = Double.valueOf(rezultat);

        double suminsurancerate = pensionAndDisabilityInsuranceRate+healthInsuranceFundRate+additionalHealthInsuranceAndPersonalInjuryInsuranceRate+unemploymentInsuranceFundRate;
        double netotobrutorate = 1- suminsurancerate - (1 - suminsurancerate)* personalIncomeTaxRate;
        double bruto = ((neto-(personalIncomeTaxExemptionPerMonth/10))/netotobrutorate);

        double helthPension =
                bruto*pensionAndDisabilityInsuranceRate//penzisko
                        + bruto*healthInsuranceFundRate//zdravstveno
                        + bruto*additionalHealthInsuranceAndPersonalInjuryInsuranceRate//dop zdrav
                        + bruto*unemploymentInsuranceFundRate;//nevrab




        double personalIncomeTax = (bruto -(helthPension+ personalIncomeTaxExemptionPerMonth))*personalIncomeTaxRate;


        netlist = new ArrayList<>();
        netlist.add(String.valueOf(Math.round(neto)));
        netlist.add(String.valueOf(Math.round(bruto*pensionAndDisabilityInsuranceRate)));
        netlist.add(String.valueOf(Math.round(bruto*healthInsuranceFundRate)));
        netlist.add(String.valueOf(Math.round(bruto*additionalHealthInsuranceAndPersonalInjuryInsuranceRate)));
        netlist.add(String.valueOf(Math.round(bruto*unemploymentInsuranceFundRate)));
        netlist.add(String.valueOf(Math.round(helthPension)));
        netlist.add(String.valueOf(Math.round(personalIncomeTax)));
        netlist.add(String.valueOf(Math.round(helthPension+personalIncomeTax)));
        netlist.add(String.valueOf(Math.round(bruto)));




    }

}
