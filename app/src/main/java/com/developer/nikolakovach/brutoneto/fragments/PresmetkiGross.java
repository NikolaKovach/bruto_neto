package com.developer.nikolakovach.brutoneto.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.developer.nikolakovach.brutoneto.R;
import com.developer.nikolakovach.brutoneto.calcs.Gross;

import java.util.ArrayList;
import java.util.Currency;

/**
 * A simple {@link Fragment} subclass.
 */
public class PresmetkiGross extends Fragment {

TextView brutocalc, netocalc, pension,health, unemploy, addhealth, suminsurance, sumcontry,persontax,currency, currency1;
View v;
ArrayList arrayList;


    public PresmetkiGross() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_presmetki, container, false);

        initcomponents();
        setcomponents();


        return v;
    }

    private void setcomponents() {
        arrayList = Gross.getList();
        brutocalc.setText(arrayList.get(0).toString());
        pension.setText(arrayList.get(1).toString()+" Ден.");
        health.setText(arrayList.get(2).toString()+" Ден.");
        unemploy.setText(arrayList.get(4).toString()+" Ден.");
        addhealth.setText(arrayList.get(3).toString()+" Ден.");
        suminsurance.setText(arrayList.get(5).toString()+" Ден.");
        persontax.setText(arrayList.get(6).toString()+" Ден.");
        sumcontry.setText(arrayList.get(7).toString()+" Ден.");
        netocalc.setText(arrayList.get(8).toString());
        currency.setText(" Ден.");
        currency1.setText(currency.getText().toString());


    }

    private void initcomponents() {
        brutocalc = v.findViewById(R.id.brutoCalc);
        netocalc = v.findViewById(R.id.netocalc);
        pension = v.findViewById(R.id.pension);
        health = v.findViewById(R.id.health);
        unemploy = v.findViewById(R.id.unempluy);
        addhealth = v.findViewById(R.id.addhelth);
        suminsurance = v.findViewById(R.id.sumins);
        persontax = v.findViewById(R.id.pertax);
        sumcontry = v.findViewById(R.id.sumcontrty);
        currency = v.findViewById(R.id.currency);
        currency1 = v.findViewById(R.id.currency1);

    }

}
