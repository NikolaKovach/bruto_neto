package com.developer.nikolakovach.brutoneto.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.developer.nikolakovach.brutoneto.R;
import com.numetriclabz.numandroidcharts.ChartData;
import com.numetriclabz.numandroidcharts.PieChartL;


import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PieChart extends Fragment {
    View v;
    PieChartL pie;


    public PieChart() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_pie_chart, container, false);

        pie= v.findViewById(R.id.pie_chart);



        List<ChartData> values = new ArrayList<>();
                values.add(new ChartData("Cricket", 50f));
                values.add(new ChartData("Football", 60f));
                values.add(new ChartData("Hockey", 30f));
                values.add(new ChartData("Tenis", 20f));
                values.add(new ChartData("Rugby",40f));
                values.add(new ChartData("Polo",10f));
                pie.setData(values);



        return v;
    }


}
